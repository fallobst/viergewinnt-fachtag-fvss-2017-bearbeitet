package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.FeldStatus;
import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Position;
import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Spielfeld;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JPanel;
/**
 *
 * @author Jonas Greifenhain (documentation)
 * @author Philip (code)
 */

public class SpielfeldGui extends JPanel
{   
    private int letzteSpalte = -1;
    private JButton buttons [][] = new JButton[7][5];
    private ActionListener spalten [] = new ActionListener[7];
    private Set<ActionListener> _actionListener;
    
    public SpielfeldGui()
    {
        //JPanel initialisieren
        super();
        GridLayout experimentLayout = new GridLayout(5,7,20, 20);
        setLayout(experimentLayout);
        setBackground(Color.black);
    
        for(int x=0; x<7; x++)
        {
            final int spalte = x;
            
            ActionListener al = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    letzteSpalte = spalte;
                }
            };
            for(int y = 0; y <5; ++y)
            {
                buttons[x][y] = new JButton("" + x + "; " + y);
                buttons[x][y].addActionListener(al);
            }
        }
        
        for(int y = 0; y <5; ++y)
        {
            for(int x=0; x<7; x++)
            {
                add(buttons[x][y]);
            }
        }
        _actionListener = new HashSet<>();
    }
    
    /**
     * Diese Funktion stellt den aktuellen Zustand von "spielfeld" un der GUI
     * dar.
     * 
     * @param spielfeld Das darzustellende Spielfeld
     */
    public void zeigeSpielfeld(Spielfeld spielfeld)
    {
        for(int x=0; x<7; x++)
        {
            for(int y = 0; y <5; ++y)
            {
                int status = spielfeld.getWert(new Position(x,y));
                
                if (status==FeldStatus.UNBELEGT)
                {
                     buttons[x][y].setBackground(Color.white);
                }
                
                if (status==FeldStatus.GEGENSPIELER)
                {
                     buttons[x][y].setBackground(Color.red);
                }
                
                if (status==FeldStatus.SPIELER)
                {
                     buttons[x][y].setBackground(Color.green);
                }
            }
        }
    }
    
    /**
     * Diese Funktion gibt die zuletzt vom Spieler ausgewählte Spalte zurück.
     *   0 ist dabei ganz links.
     * 
     * @return die zuletzt vom Spieler ausgewählte Spalte.
     */
    public int getGewaehlteSpalte()
    {
        return letzteSpalte;
    }
    
    /**
     * Der hinzuzufügende ActionListener schlägt an, wenn der Spieler eine
     * Spalte ausgewählt hat.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addSpaltenwahlActionListener(ActionListener al)
    {
        _actionListener.add(al);
    }
}