/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Spielfeld;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Jonas Greifenhain (documentation)
 * @author Philip (code)
 */
public class MainGui
{
    private final SpielfeldGui _spielfeldGui;
    private final ChatGui _chatGui;
    private final StartGui _startGui;
    
    private final JFrame _fenster;
   
    public MainGui()
    {
        _spielfeldGui = new SpielfeldGui();
        _chatGui = new ChatGui();
        _fenster = new JFrame("Vier Gewinnt");
        
        _fenster.setLayout(new BorderLayout());
        _fenster.add(_spielfeldGui, BorderLayout.CENTER);
        _fenster.add(_chatGui, BorderLayout.LINE_END);
        
        _fenster.setSize(1030, 520);
        _fenster.show();
        
        _startGui = new StartGui(_fenster);
        
    }
    
    /**
     * Diese Funktion stellt den aktuellen Zustand von "spielfeld" un der GUI
     * dar.
     * 
     * @param spielfeld Das darzustellende Spielfeld
     */
    public void zeigeSpielfeld(Spielfeld spielfeld)
    {
        _spielfeldGui.zeigeSpielfeld(spielfeld);
    }
    
    /**
     * Diese Funktion gibt die zuletzt vom Spieler ausgewählte Spalte zurück.
     *   0 ist dabei ganz links.
     * 
     * @return die zuletzt vom Spieler ausgewählte Spalte.
     */
    public int getGewaehlteSpalte()
    {
        return _spielfeldGui.getGewaehlteSpalte();
    }
    
    /**
     * Der hinzuzufügende ActionListener schlägt an, wenn der Spieler eine
     * Spalte ausgewählt hat.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addSpaltenwahlActionListener(ActionListener al)
    {
        _spielfeldGui.addSpaltenwahlActionListener(al);
    }
    
    /**
     * Diese Nachricht gibt eine neu geschriebene ChatNachricht des Spielers
     * zurück.
     * 
     * @return Die Nachricht des Spielers
     */
    public String getChatText()
    {
        return _chatGui.getChatText();
    }
    
    /**
     * Diese Funktion fügt zu dem Chatverlauf eine Nachricht hinzu, die der
     * Gegner geschrieben hat.
     * 
     * @param text Die Nachricht vom Gegner
     */
    public void addChatText(String text)
    {
        _chatGui.addChatText(text);
    }
    
    /**
     * Der hinzuzufügende ActionListener wird anschlagen, wenn der Benutzer eine
     * Chat-Nachricht senden möchte.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addChatActionListener(ActionListener al)
    {
        _chatGui.addChatActionListener(al);
    }
    
    /**
     * Diese Funktion gibt zurück, ob das Spiel als Client oder Server arbeiten
     * soll.
     * 
     * false = Verbindung aufbauen
     * true  = Auf Verbindung warten (Server-Modus)
     * 
     * @return Der Verbindungsmodus
     */
    public boolean getConnectionMode()
    {
        return _startGui.getConnectionMode();
    }
    
    /**
     * Diese Funktion gibt die Adresse des Servers zurück, wenn sich das Spiel
     * mit einem anderen Spieler verbinden soll.
     * (getConnectionMode() == false) -> Verbindung aufbauen
     * 
     * @return 
     */
    public String getHost()
    {
        return _startGui.getHost();
    }
    
    /**
     * Diese Funktion gibt den angegebenen Port zurück
     * 
     * @return Der angegebene Port
     */
    public int getPort()
    {
        return _startGui.getPort();
    }
    
    /**
     * Der hinzuzufügende ActionListener wird anschlagen, wenn der Benutzer eine
     * Verbindung aufbauen will.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addVerbindenActionListener(ActionListener al)
    {
        _startGui.addVerbindenActionListener(al);
    }
}