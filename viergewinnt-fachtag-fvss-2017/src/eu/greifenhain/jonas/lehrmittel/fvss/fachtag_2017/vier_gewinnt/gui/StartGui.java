package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

/**
 *
 * @author Jonas Greifenhain
 */
public class StartGui extends JDialog
{
    private final JCheckBox _serverModeCheckBox;
    private final JTextField _serverAddressTextField;
    private final JSpinner _portSpinner;
    private final JButton _startButton;
    
    public StartGui(JFrame uebergeordnet)
    {
        //Initialisieren der übergeordneten Klasse (JFrame)
        super(uebergeordnet, true);
        setTitle("Vier Gewinnt starten");
        setLayout(new SpringLayout());
        _serverModeCheckBox = new JCheckBox("Server-Modus");
        _serverAddressTextField = new JTextField();
        _portSpinner = new JSpinner();
        _startButton = new JButton("Starten");
        createJFrame();
        setSize(300, 150);
        show();
    }
    
    private void createJFrame()
    {
        add(new JLabel("Server-Modus"));
        add(_serverModeCheckBox);
        add(new JLabel("Server-Adresse"));
        add(_serverAddressTextField);
        add(new JLabel("Server-Port"));
        add(_portSpinner);
        add(new JPanel());
        add(_startButton);
        
        SpringUtilities.makeCompactGrid(getContentPane(),
                                4, 2, //rows, cols
                                6, 6,        //initX, initY
                                6, 6);       //xPad, yPad
        guiModeListener();
    }
    
    private void guiModeListener()
    {
        _serverModeCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(_serverModeCheckBox.isSelected())
                    _serverAddressTextField.setEnabled(false);
                else
                    _serverAddressTextField.setEnabled(true);
            }
        });
    }
    
    /**
     * Diese Funktiion gibt zurück, ob das Spiel als Client oder Server arbeiten
     * soll.
     * 
     * false = Verbindung aufbauen
     * true  = Auf Verbindung warten (Server-Modus)
     * 
     * @return Der Verbindungsmodus
     */
    public boolean getConnectionMode()
    {
        return _serverModeCheckBox.isSelected();
    }
    
    /**
     * Diese Funktion gibt die Adresse des Servers zurück, wenn sich das Spiel
     * mit einem anderen Spieler verbinden soll.
     * (getConnectionMode() == false) -> Verbindung aufbauen
     * 
     * @return 
     */
    public String getHost()
    {
        return _serverAddressTextField.getText();
    }
    
    /**
     * Diese Funktion gibt den angegebenen Port zurück
     * 
     * @return Der angegebene Port
     */
    public int getPort()
    {
        return (int)_portSpinner.getValue();
    }
    
    /**
     * Der hinzuzufügende ActionListener wird anschlagen, wenn der Benutzer eine
     * Verbindung aufbauen will.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addVerbindenActionListener(ActionListener al)
    {
        _startButton.addActionListener(al);
    }
}
