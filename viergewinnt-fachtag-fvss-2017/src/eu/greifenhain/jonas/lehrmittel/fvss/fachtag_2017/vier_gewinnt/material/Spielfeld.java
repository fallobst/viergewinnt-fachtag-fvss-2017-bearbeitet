package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jonas Greifenhain (documentation)
 * @author Till (code)
 * 
 * Das Spielfeld sollte 7 Reihen und 5 Zeilen haben.
 */
public class Spielfeld
{
    int[][] Spielfeld = new int[5][7];
    
    public Spielfeld()
    {
        for(int i=0; i<7; i++)
        {
            for(int x=0; x<5; x++)
            {
                Spielfeld[x][i]=FeldStatus.UNBELEGT;                   
            }
        }
            
    }

    /**
     * Mit dieser Funktion wird der Zustand des oberen Feldes in einer Spalte
     * verändert.
     * Die Zustände werden den Konstanten von "FeldStatus" entnommen.
     * Diese Funktion darf nur benutzt werden, wenn
     * getViererReihe() == null
     * 
     * Beispiel:
     * setPosition(position, FeldStatus.SPIELER)
     * 
     * @param position Die Position des zu verändernden Feld.
     * @param state Der Zustand des Feldes.
     * 
     * @return War noch Platz in der Spalte
     */
    public boolean setSpalte(int spalte, int state)
    {
            for(int x=4; x>=0; x--)
            {
                if(Spielfeld[x][spalte]==FeldStatus.UNBELEGT)
                {
                    Spielfeld[x][spalte]=state;
                    return true;
                }
            }   
            return false;
    }
    
    /**
     * Diese Funktion gibt den Zustand einer Position zurück. Der Zustand ist
     * eine Konstante aus "FeldStatus".
     * 
     * @param position Die abzufragende Position
     * @return Der Zustand an "position"
     */
    public int getWert(Position position)
    {
        return Spielfeld[position.getY()][position.getX()];
    }
    
    /**
     * Wenn ein Spieler gewonnen hat, gibt diese Funktion eine Liste mit
     * den Positionen zurück, die eine Vierer-Reihe ergeben.
     * Hat kein Spieler gewonnen, gibt die Funktion null zurück.
     * 
     * @return Die Sieg-Vierer-Reihe, wenn möglich
     */
    public List<Position> getViererReihe()
    {
        List<Position> ergebnis=new ArrayList();
        
        ergebnis.addAll(sucheSpalte());
        ergebnis.addAll(sucheZeile());
        ergebnis.addAll(sucheSpalteZeile());
        
        return ergebnis;
    }
    
    private List<Position> sucheSpalte()
    {
        List<Position> ergebnis=new ArrayList();
        
        for(int i=0; i<7; i++)
        {
            for(int x=0; x<5; x++)
            {
                int status=Spielfeld[x][i];
                
                int s = i;
                for(; s<i+4 && s<7; s++)
                {
                    if(Spielfeld[x][s]!=status)
                    {
                        s=s-1;
                        break;
                        
                    }
                }
                
                if(s-i==4)
                {
                    for(int ii=i; ii<s; ii++)
                    {
                        ergebnis.add(new Position(ii,x));
                    }
                }
            }
        }
        return ergebnis;
    }
    
    private List<Position> sucheZeile()
    {
        List<Position> ergebnis=new ArrayList();
       for(int i=0; i<7; i++)
        {
            for(int x=0; x<5; x++)
            {
                int status=Spielfeld[x][i];
                
                int s = x;
                for(; s<x+4 && s<7; s++)
                {
                    if(Spielfeld[s][i]!=status)
                    {
                        s=s-1;
                        break;
                        
                    }
                }
                
                if(s-x==4)
                {
                    for(int ii=x; ii<s; ii++)
                    {
                        ergebnis.add(new Position(i,ii));
                    }
                }
            }
        }
       return ergebnis;
    }
    
    private List<Position> sucheSpalteZeile()
    {
        List<Position> ergebnis=new ArrayList();
        for(int i=0; i<7; i++)
        {
            for(int x=0; x<5; x++)
            {
                int status=Spielfeld[x][i];
                
                int s = x;
                int z = i;
                for(; s<x+4 && s<7 && z<i+4 && z<4; s++)
                {
                    if(Spielfeld[s][z]!=status)
                    {
                        s=s-1;
                        z=z-1;
                        break;
                        
                    }
                    z++;
                }
                
                if(s-x==4)
                {
                    for(int ii=x; ii<s; ++ii)
                    {
                        for(int iii=i; iii<z; ++iii)
                        {
                           ergebnis.add(new Position(iii, ii)); 
                        }
                    }
                }
            }
        }
    return ergebnis;
    }
}