package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * 
 * @author Jonas Greifenhain (documentation)
 * @author Philip (code)
 */
public class ChatGui extends JPanel
{
    JButton sendenButton;
    JTextArea chatTextFeld;
    JTextField sendenFeld;
    
    public ChatGui()
    {
        //JPanel initialisieren
        super();
        setLayout(new BorderLayout());
        sendenButton = new JButton("senden");
        chatTextFeld = new JTextArea();
        sendenFeld = new JTextField();
        add(sendenButton,BorderLayout.PAGE_START);
        add(chatTextFeld,BorderLayout.CENTER);
        add(sendenFeld,BorderLayout.PAGE_END);
        setPreferredSize(new Dimension(300, 300));
    }
    
    /**
     * Diese Nachricht gibt eine neu geschriebene ChatNachricht des Spielers
     * zurück.
     * 
     * @return Die Nachricht des Spielers
     */
    public String getChatText()
    {
        String s = sendenFeld.getText();
        sendenFeld.setText("");
        return s;
    }
    
    /**
     * Diese Funktion fügt zu dem Chatverlauf eine Nachricht hinzu, die der
     * Gegner geschrieben hat.
     * 
     * @param text Die Nachricht vom Gegner
     */
    public void addChatText(String text)
    {
        String s = chatTextFeld.getText();
        s = s + text;
        chatTextFeld.setText(s);
    }
    
    /**
     * Der hinzuzufügende ActionListener wird anschlagen, wenn der Benutzer eine
     * Chat-Nachricht senden möchte.
     * 
     * @param al Der hinzuzufügende ActionListener
     */
    public void addChatActionListener(ActionListener al)
    {
        sendenButton.addActionListener(al);
        
    }
}