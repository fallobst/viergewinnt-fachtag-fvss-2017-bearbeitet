package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.tools;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Nachricht;

/**
 *
 * @author Jonas Greifenhain (documentation)
 * @author Jan, Taha (code)
 */
public class Parser
{
    /**
     * Diese Funktion wertet einen Text vom Gegenspieler aus, sodass er
     * weiterverarbeitet werden kann.
     * 
     * @param nachricht Der Text, der vom Gegner empfangen wurde
     * @return Die ausgewertete Nachricht
     */
    public Nachricht parseNachricht(String nachricht)
    {
        if(nachricht.substring(0, 3).equals(Nachricht.CHAT + "xx")){
            Nachricht message = new Nachricht(Nachricht.CHAT, nachricht.substring(3));
            return message;
        }else{
            Nachricht message = new Nachricht(Nachricht.POSITION, Integer.parseInt(nachricht.substring(3)));
            return message;
        }
        
    }
    
    /**
     * Diese Funktion wandelt eine Nachricht in einen Text um, sodass sie über das
     * Netzwerk gesendet werden kann.
     */
    public String parseNachricht(Nachricht nachricht)
    {
        String text, pos;
        if(nachricht.getType() == Nachricht.CHAT){
            text = "1xx" + (String) nachricht.getParameter();
            return text;
        }else{      //POS
            pos = "0xx" +((Integer)nachricht.getParameter()).toString();
            return pos;
        }
    }
}