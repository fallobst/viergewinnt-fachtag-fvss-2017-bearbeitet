package eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.tools;

import eu.greifenhain.jonas.lehrmittel.fvss.fachtag_2017.vier_gewinnt.material.Nachricht;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jonas Greifenhain (documentation)
 * @author Jan, Taha (code)
 */
public class Netzverbindung
{
    private final Parser _parser;
    private Socket socket;
    private PrintWriter printwriter;
    private BufferedReader in;
    /**
     * Mit diesem Constructor wird verbindet sich das erzeugte Objekt mit einem
     * Spielserver. - CLIENT
     * 
     * @param host Die Adresse des Spielservers
     * @param port Der Port des Spielservers
     */
    public Netzverbindung(String host, int port)
    {
        _parser = new Parser();
        try {
            socket = new Socket(host, port);
            printwriter = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        } catch (IOException ex) {
            System.out.println("Connection failed!!!");
        }   
    }
    
    /**
     * Mit diesem Constructor wird das erzeugte Object zum Spielserver. - SERVER
     * Der Construktor wartet auf eine Verbindung.
     * 
     * @param port Der Port, auf dem der Server gestartet werden soll
     */
    public Netzverbindung(int port)
    {
        _parser = new Parser();
        try {
            ServerSocket server = new ServerSocket(port);
            socket = server.accept();
            printwriter = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
        } catch (IOException ex) {
            System.out.println("Connection failed!!!");
        }
        
    }
    
    /**
     * Diese Funktion sendet eine Nachricht
     * 
     * @param nachricht Die Nachricht, die gesendet werden soll
     */
    public void sendeNachricht(Nachricht nachricht)
    {
        printwriter.println(_parser.parseNachricht(nachricht) + "\n");
    }
    
    /**
     * Diese Funktion gibt eine Liste mit alles Nachrichten, die seit dem
     * letzten Aufrufen empfangen wurden, zurück.
     * 
     * @return Die Liste der Nachrichten
     */
    public List<Nachricht> getLetzteNachrichten()
    {
        ArrayList<Nachricht> dataList = new ArrayList<>();
        try {
            while(in.ready()){
                dataList.add(_parser.parseNachricht(in.readLine()));
            }
        } catch (IOException ex) {
            System.err.println("FAIL!!!! TAHA IST SCHULD");
        }
        return dataList;
    }
}